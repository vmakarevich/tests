var assert = require('assert');
var {sum, multiply, minus, divide} =  require('./functions');

describe('Sum', function () {

    it('should sum 2+2=4', () => {
      assert.equal(sum(2, 2), 4)
    })

});

describe('Multiply', function () {

    it('should multiply 2*3=6', () => {
        assert.equal(multiply(2, 3), 6)
    })

});

describe('Minus', function () {

    it('should minus 2-3=-1', () => {
        assert.equal(minus(2, 3), -1)
    })

});

describe('Divide', function () {

    it('should divide 2/4=0.5', () => {
        assert.equal(divide(2, 4), 0.5)
    })

});
