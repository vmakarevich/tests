function sum(a, b) {
  return a+b
}

function multiply(a, b) {
  return a*b
}

function minus(a, b) {
  return a-b
}

function divide(a, b) {
    return parseFloat(a)/b
}

module.exports = {
  sum, multiply, minus, divide
};
